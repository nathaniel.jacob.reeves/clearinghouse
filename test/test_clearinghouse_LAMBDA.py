import requests
import datetime
import pytest

# myobj = {
#     "merchant_name" : "Fox Books",
#     "merchant_token" : "fox1wekrj",
#     "bank" : "Wells Fargo",
#     "cc_num" : "12345",
#     "card_type" : "Debit",
#     "security_code" : "uwef889",
#     "amount" : "17.15",
#     "card_zip" : "84765",
#     "timestamp" : timestamp
# }

# x = requests.post(url, json = myobj)

# print(x.text)

todo = pytest.mark.skip("todo")

def describe_clearing_house():
    
    @pytest.fixture
    def url():
        return 'https://9au0x1ibc3.execute-api.us-west-1.amazonaws.com/v1/clearingHouseAPI'
    
    @pytest.fixture
    def timestamp():
        return str(datetime.datetime.now())
    
    @pytest.fixture
    def credit_card_transaction_template(timestamp):
        return {
            "merchant_name" : "",
            "merchant_token" : "",
            "bank" : "",
            "cc_num" : "",
            "card_type" : "Credit",
            "security_code" : "",
            "amount" : "",
            "card_zip" : "",
            "timestamp" : timestamp
        }
    
    @pytest.fixture
    def debit_card_transaction_template(timestamp):
        return {
            "merchant_name" : "",
            "merchant_token" : "",
            "bank" : "",
            "cc_num" : "",
            "card_type" : "Debit",
            "security_code" : "",
            "amount" : "",
            "card_zip" : "",
            "timestamp" : timestamp
        }
    
    @todo
    def it_will_accept_a_good_merchant():
        pass
        
    @todo
    def it_will_not_accept_a_bad_merchant():
        pass
        
    @todo
    def it_will_not_accept_an_incorrect_bank_account():
        pass
        
    @todo
    def it_will_not_accept_an_incorrect_bank_name():
        pass
        
    @todo
    def it_will_accept_a_valid_credit_card():
        pass
    
    @todo
    def it_will_not_accept_an_invalid_credit_card():
        pass
        
    @todo
    def it_will_accept_a_valid_debit_card():
        pass
    
    @todo
    def it_will_not_accept_an_invalid_credit_card():
        pass