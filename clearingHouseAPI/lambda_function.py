import json
import datetime
import boto3
import random

dynamodb = boto3.resource('dynamodb')

def lambda_handler(event, context):
    
    # Simulate Bank Failure 10% of the time
    # bank_not_avalible = random.randint(1,10)
    # if bank_not_avalible <= 9:
    #     return format_return("Bank Not Available")
    
    # Get data from the event
    if (event['body']) and (event['body'] is not None):
        body = json.loads(event['body'])
        merchant_name = body['merchant_name']
        token = body['merchant_token']
        bank = body['bank']
        cc_num = body['cc_num']
        card_type = body['card_type']
        security_code = body['security_code']
        amount = body['amount']
        card_zip = body['card_zip']
        timestamp = body['timestamp']
    else:
        return format_return("There was an error in your post, no body present.")
        
    # Get the Merchant Table
    merchant_table = dynamodb.Table('Merchant')
    
    if not merchant_auth(merchant_name, token, merchant_table):
        return format_return("Bad Merchant Credentials.")
    
    # Reject transactions without a valid security code
    if len(security_code) < 3:
        return format_return('Declined, No Security Code')
    
    
    if card_type == "Debit":
        bank_table = dynamodb.Table('BankAccounts')
        text = update_bank(bank,cc_num,card_type,amount,bank_table)
    else:
        bank_table = dynamodb.Table('CreditAccounts')
        text = update_bank(bank,cc_num,card_type,amount,bank_table)
    
    # Write to the transaction Table
    transactions = dynamodb.Table('Transactions')
    insert_transaction(transactions, text, amount, cc_num, token, merchant_name, card_zip, timestamp)
    
    return format_return(text)
        
def insert_transaction(transaction_table, status, amount, account_num, merchant_id, merchant_name, zip, timestamp):
    transaction_table.put_item(
        Item={
            'MerchantID': str(merchant_id),
            'MerchantName': str(merchant_name),
            'lastFour': str(account_num[-4:]),
            'amount': str(amount),
            'datetime': str(datetime.datetime.now()),
            'status': str(status),
            'zip': str(zip),
            'timestamp': str(timestamp)
        }
    )
    
    
def update_bank(bank, cc_num, card_type, amount, bank_table):
    try:
        # Retrieve the bank account info from the bank_table
        response = bank_table.get_item(
            Key={
                'BankName': bank,
                'AccountNum': cc_num
            },
            ConsistentRead=True
        )
            
        # Ensure there is something in the response   
        if 'Item' not in response:
            return 'Bad Bank Credentials.'
            
        if card_type == 'Debit':    
            balance = float(response['Item'].get('Balance', {'N': '0'}))
            if float(amount) > balance:
                return 'Insufficient funds.'
            else:
                balance -= float(amount)
                update_balance(bank_table, bank, cc_num, str(balance))
                return 'Approved.'
        else:
            credit_limit = float(response['Item'].get('CreditLimit', {'N': '0'}))  
            credit_used = float(response['Item'].get('CreditUsed', {'N': '0'}))    
            if float(amount) > (credit_limit - credit_used):
                return 'Insufficient funds.'
            else:
                credit_used += float(amount)
                update_credit(bank_table, bank, cc_num, str(credit_used))
                return 'Approved.'

    except Exception as e:
        print('Error: ', e)
        return 'Error: ' + str(e)


def update_balance(bank_table,bank,cc_num,new_balance):
    bank_table.update_item(
        Key={
            'BankName': bank,
            'AccountNum': cc_num
        },
        UpdateExpression='SET Balance = :val',
        ExpressionAttributeValues={
            ':val': new_balance
        }
    )
    
    
def update_credit(credit_table, bank, cc_num, credit_used):
    credit_table.update_item(
        Key={
            'BankName': bank,
            'AccountNum': cc_num
        },
        UpdateExpression='SET CreditUsed = :val',
        ExpressionAttributeValues={
            ':val': credit_used
        }
    )
    
def merchant_auth(name,token,merchant_table):
    response = merchant_table.get_item(
        Key={
            'MerchantName': name,
            'Token': token
        })
    if 'Item' not in response:
        return False
    return True

def format_return(return_val):
    result = {
        "statusCode": 200,
        "headers": {
            "Content-Type": "*/*"
        },
        "body": json.dumps(return_val)
    }
    return result
    
def echo_back(response):
    result = {
        "statusCode": 200,
        "headers": {
            "Content-Type": "*/*"
        },
        "body": json.dumps(response)
    }
    return result
    
